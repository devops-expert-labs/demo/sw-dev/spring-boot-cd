package com.example.realworld.domain.aggregate.article.repository;

import com.example.realworld.domain.aggregate.article.entity.Favorite;
import com.example.realworld.domain.aggregate.article.entity.FavoriteId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FavoriteRepository extends JpaRepository<Favorite, FavoriteId> {
    Optional<Favorite> findByArticleIdAndUserId(Long articleId, Long authorId);

    Long countByArticleId(Long articleId);
}
