# GitOps for Realworld

## Namespace

환경별 네임스페이스를 생성합니다.

```bash
kubectl create ns dev
kubectl create ns prod
```

## Secrets

각 환경별 네임스페이스에 필요한 시크릿을 생성합니다.

### Image Pull Secret

```bash
kubectl create secret docker-registry image-pull-secret \
  --docker-server=registry.gitlab.com \
  --docker-username=${DEPLOYER_USERNAME} \
  --docker-password=${DEPLOYER_TOKEN} \
  --docker-email=devops@infograb.net \
  -n ${NAMESPACE}
```

### Database Credentials

```bash
kubectl create secret generic db-credentials \
  --from-literal=DB_NAME=${POSTGRES_DB} \
  --from-literal=DB_USER=${POSTGRES_USER} \
  --from-literal=DB_PASSWD=${POSTGRES_PASSWORD} \
  -n ${NAMESPACE}
```

## Deploy NGINX Ingress Controller

NGINX Ingress Controller가 설치되어 있지 않다면 설치합니다.

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx \
  --create-namespace
```

## Deploy cert-manager

**cert-manager**가 설치되어 있지 않다면 설치합니다.

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm upgrade --install cert-manager jetstack/cert-manager \
  --repo https://charts.jetstack.io \
  --namespace cert-manager \
  --create-namespace \
  --version v1.11.0 \
  --set installCRDs=true
```

## Let's Encrypt Wildcard ClusterIssuer

### ClusterIssuer

Let's Encrypt Wildcard 인증서를 발급받기 위한 ClusterIssuer를 생성합니다.

```bash
cat << EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-wildcard-cicd-plumbing
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    preferredChain: "ISRG Root X1"
    email: devops@infograb.net
    privateKeySecretRef:
      name: letsencrypt-wildcard-cicd-plumbing
    solvers:
    - selector:
        dnsNames:
        - "*.dev.cicd.plumbing"
        - "*.cicd.plumbing"
      dns01:
        route53:
          region: ap-northeast-2
          accessKeyID: ${AWS_ACCESS_KEY_ID}
          #accessKeyIDSecretRef:
          #  name: aws-route53-credentials
          #  key: AWS_ACCESS_KEY_ID
          secretAccessKeySecretRef:
            name: aws-route53-credentials
            key: AWS_SECRET_ACCESS_KEY
EOF
```

### Certificate

`*.dev.cicd.plumbing` 도메인에 대한 Let's Encrypt Wildcard 인증서를 발급받기 위한 Certificate를 생성합니다.

```bash
cat << EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: letsencrypt-wildcard-dev-cicd-plumbing
  namespace: realworld-dev
spec:
  secretName: dev-cicd-plumbing-tls
  issuerRef:
    name: letsencrypt-wildcard-cicd-plumbing
    kind: ClusterIssuer
    group: cert-manager.io
  dnsNames:
  - "*.dev.cicd.plumbing"
EOF
```

`*.cicd.plumbing` 도메인에 대한 Let's Encrypt Wildcard 인증서를 발급받기 위한 Certificate를 생성합니다.

```bash
cat << EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: letsencrypt-wildcard-cicd-plumbing
  namespace: realworld-prod
spec:
  secretName: cicd-plumbing-tls
  issuerRef:
    name: letsencrypt-wildcard-cicd-plumbing
    kind: ClusterIssuer
    group: cert-manager.io
  dnsNames:
  - "*.cicd.plumbing"
EOF
```

## Deploy Realworld using Helm

```bash
helm upgrade --install realworld-dev ./realworld-chart \
  --namespace ${NAMESPACE} \
  --values ./realworld-chart/values.yaml \
  --values ./realworld-chart/values-dev.yaml \
  --set api.image.tag=${API_IMAGE_TAG} \
  --set app.image.tag=${APP_IMAGE_TAG} \
  --dry-run \
  --debug
```

## Delete Realworld using Helm

```bash
helm delete realworld-dev --namespace ${NAMESPACE}
```
